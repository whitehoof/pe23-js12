"use strict"

/*
ТЕОРІЯ:
Чому для роботи з input не рекомендується використовувати клавіатуру?

Тому що в input можна вводити щось не лише клавіатурою, а й мишою чи трекпадом (right-click & paste), голосом, рукописним вводом (смартфони зі стилусами, графічні планшети, трекпади) та іншими відносно новими способами. Якщо на input повісити слухача саме клавіатурних подій, скрипт не помітить введення в input не-клавіатурними способами.




ПРАКТИКА:
*/

const keys = document.querySelector(".btn-wrapper").children;

// вішаю слухач клавіатурних подій
document.addEventListener( "keydown", e => {
	// за кожного keydown'а перебираю всі кнопки
	[...keys].forEach( el => {
		// фарбую в чорний всі кнопки
		el.style.backgroundColor = "black";
		// фарбую в синій ті кнопки, innerText яких співпадає з keyName(event) (функцію див.нижче)
		if (el.innerText === keyName(e)) {
			el.style.backgroundColor = "blue";
		};
	});
});

// функція повертає event.code без частини рядка "Key" та "Digit".
function keyName(event) {
	// перевірка, чи справді то event, що передано в аргументі
	if (event instanceof Event) {
		if (event.code.startsWith("Key")) {
			return event.code.substring(3);
		} else if (event.code.startsWith("Digit")) {
			return event.code.substring(5);
		} else {
			return event.code;
		};
	} else { 
		throw new Error("The argument submitted to keyName() function is not an event!");
	};
};
